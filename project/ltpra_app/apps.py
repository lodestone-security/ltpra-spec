from django.apps import AppConfig


class LtpraAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ltpra_app'
